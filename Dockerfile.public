FROM node:14-alpine as client_builder
COPY public-client /tmp/public-client/
COPY common /tmp/common/
WORKDIR /tmp/public-client/
# TODO: Production webpack config
RUN npm ci && npm run build

FROM python:3.8-alpine as python_builder

RUN apk add build-base
WORKDIR /work
COPY public-backend/requirements.txt .
RUN sed -n '/^brotli/p' <requirements.txt > constraint.txt \
      && pip download brotli -c constraint.txt --no-binary :all: \
      && unzip Brotli-* \
      && rm *.zip \
      && (cd Brotli-* && python setup.py bdist_wheel)

RUN apk add postgresql-dev \
      && pip download psycopg2==2.8.5 --no-binary :all: \
      && tar -xf psycopg2-* \
      && rm *.tar.gz \
      && (cd psycopg2-* && python setup.py bdist_wheel)

FROM python:3.8-alpine

RUN apk add postgresql-libs

COPY --from=client_builder /tmp/public-client/dist /tmp/public-client/dist/
COPY --from=client_builder /tmp/public-client/webpack-stats.json /tmp/public-client/

COPY --from=python_builder /work/Brotli-*/dist/Brotli-*.whl /tmp/
COPY --from=python_builder /work/psycopg2-*/dist/psycopg2-*.whl /tmp/

COPY public-backend/requirements.txt /tmp/requirements.txt
RUN pip install waitress /tmp/Brotli-*.whl /tmp/psycopg2-*.whl -r /tmp/requirements.txt

ENV \
  PYTHONPATH=/srv/public-backend/src \
  DJANGO_SETTINGS_MODULE=planner_public_backend.settings.prod

RUN printf \
      '#!/bin/sh\nexec python /srv/public-backend/manage.py migrate' \
      > /usr/local/bin/migrate && \
      chmod +x /usr/local/bin/migrate

COPY public-backend /srv/public-backend
COPY common /srv/common/
WORKDIR /srv/public-backend

RUN env ALLOWED_HOSTS= SECRET_KEY="irrelevant" EMAIL_URL=consolemail:// ADMINS= \
      ERROR_EMAIL_FROM=root@localhost DATABASE_URL= PLANNER_PUBLIC_BACKEND_AUTH_TOKEN= \
      DEFAULT_EMAIL_FROM=root@localhost \
      /srv/public-backend/manage.py collectstatic

EXPOSE 8080/tcp

CMD ["waitress-serve", "--port=8080", "planner_public_backend.wsgi:application"]
