import {LitElement, unsafeCSS, css, html} from "lit-element";

import styles from './styles.cssfrag'

class ValidationFeedback extends LitElement {

    static get styles() {
        return unsafeCSS(styles);
    }

    constructor() {
        super();
        this.message = null;
    }

    static get properties() {
        return {
            message: { type: String },
        };
    }

    render() {
        return html`
            <div class="${this.message ? 'visible ' : ''}highlight">
                <div class="positioner">
                    <div class="container">
                        <div class="feedback">
                            ${this.getAttribute('message')}
                        </div>
                        <div class="arrow-down"></div>
                    </div>
                </div>
        </div>
    `;
    }
}

customElements.define("validation-feedback", ValidationFeedback);
