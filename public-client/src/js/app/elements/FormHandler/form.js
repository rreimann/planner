import {html} from 'lit-element';
import country_options from '../MultiSelect/options/countries'
import position_options from '../MultiSelect/options/positions'

export default function ({positions = []} = {positions: []}) {
    const buttons = positions.map(value => {
        const {label, url} = position_options.find(position => position.value === value) || {};
        return {label, url};
    });

    return html`<form class="main" novalidate>
    <div class="header">
        <h1>Hi, we are excited to meet you!</h1>
        <h2>To apply for a job at Seawatch please fill out the following form.</h2>
    </div>
    <div class="content">
        <fieldset aria-labelledby="personal-data-legend">
            <h2 id="personal-data-legend">Personal data</h2>
            <div class="fields">
                <div class="field field_first_name">
                    <validation-feedback></validation-feedback>
                    <label for="first_name">First Name</label>
                    <input
                            type="text"
                            name="first_name"
                            id="first_name"
                            placeholder="First Name"
                            required
                            data-validation="required"
                    />
                </div>

                <div class="field field_last_name">
                    <validation-feedback></validation-feedback>
                    <label for="last_name">Last Name</label>
                    <input
                            type="text"
                            name="last_name"
                            id="last_name"
                            placeholder="Last Name"
                            required
                            data-validation="required"
                    />
                </div>

                <div class="field field_phone_number">
                    <validation-feedback></validation-feedback>
                    <label for="phone_number">Phone Number</label>
                    <small>Please include the international prefix (like +49)</small>
                    <input type="text" name="phone_number" id="phone_number" placeholder="+49 1234/56789"
                           required
                           data-validation="required"/>
                </div>

                <div class="field field_email">
                    <validation-feedback></validation-feedback>
                    <label for="email">Email Address</label>
                    <input
                            type="email"
                            name="email"
                            id="email"
                            placeholder="you_are_awesome@example.com"
                            required
                            data-validation="required,email"
                    />
                </div>

                <div class="field narrow field_date_of_birth">
                    <validation-feedback></validation-feedback>
                    <label for="date_of_birth">Date of Birth</label>
                    <date-picker name="date_of_birth" required data-validation="required"></date-picker>
                </div>

                <div class="field narrow field_gender">
                    <validation-feedback></validation-feedback>
                    <label for="gender">Gender</label>
                    <div>
                        <select name="gender" id="gender" aria-describedby="gender_tooltip"
                                required="true"
                                data-validation="required">
                            <option value="" disabled selected hidden>Please select</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                            <option value="none_other">None/Other</option>
                            <option value="prefer_not_say">Prefer not to say</option>
                        </select>
                        <div class="tooltip-container">
                            <div
                                    id="gender_tooltip"
                                    class="tooltip"
                                    role="tooltip"
                                    aria-hidden="true"
                            >
                                We are asking because we want to make sure to have a good mix on
                                board our ships.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="field field_nationalities">
                    <validation-feedback></validation-feedback>
                    <label for="nationalities">Nationality</label>
                    <small>Please select all your nationalities</small>
                    <multi-select name="nationalities" id="nationalities" .options="${country_options}"
                                  data-validation="minOneEntry"></multi-select>
                </div>
                <div class="field wide field_spoken_languages">
                    <validation-feedback></validation-feedback>
                    <label for="spoken_languages">Spoken Languages</label>
                    <language-mapping
                            id="spoken_languages"
                            name="spoken_languages"
                            data-validation="minOneEntry"
                    ></language-mapping>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <h2>Which positions are you applying for?</h2>
            <div class="fields">
                <div class="field">
                    <label for="positions">Select positions</label>
                    <multi-select name="positions" .options="${position_options}"></multi-select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="field_motivation">
                <h2><label for="motivation">Why do you want to work for Seawatch?</label></h2>
                <validation-feedback></validation-feedback>
                <textarea-counter
                        name="motivation"
                        id="motivation"
                        maxlength="1000"
                        placeholder="Tell us what motivates you"
                        data-validation="required"
                ></textarea-counter>
            </div>
            <div class="field_qualification">
                <h2><label for="qualification">What qualifies you for the chosen positions?</label></h2>
                <p>Please refer to the job descriptions in the roles section.</p>
                <validation-feedback></validation-feedback>
                <textarea-counter
                        name="qualification"
                        id="qualification"
                        maxlength="1000"
                        placeholder="Background, fitness level, disaster/SAR experience, NGO experience, References"
                        data-validation="required"
                        .buttons="${buttons}"
                ></textarea-counter>
            </div>
        </fieldset>
        <div class="buttons">
            <input type="submit" value="Submit" class="button"/>
        </div>
    </div>
</form>`

}
