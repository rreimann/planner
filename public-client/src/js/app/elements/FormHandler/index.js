import {LitElement, css, html} from 'lit-element';

import initial_schema from 'common/schema/initialApplication';
import formHtml from './form'
import successHtml from './success.html'

import * as APIService from "../../APIService";

import validate from './validator';

class FormHandler extends LitElement {
    form = null;

    constructor() {
        super();
        this.hadSubmissionAttempt = false;
        this.sendFormData = APIService.sendFormData;

        this.notification = '';
        this.form = formHtml();
        this.data = {};
    }

    static get properties() {
        return {
            data: {type: Object},
            notification: {type: String},
        };
    }

    init() {
        this.shadowRoot.addEventListener('change', this.onInputChanged, false);

        window.addEventListener("popstate", ev => {
            if (ev.state === "success") {
                this.notification = document.createRange().createContextualFragment(successHtml);
            } else {
                this.notification = null;
            }
        });

        this.addFallbackForLabelForAttribute();

        const form = this.shadowRoot.querySelector('form');
        form.addEventListener('submit', ev => {
            ev.preventDefault();
            this.submit();
        }, false);
    }

    render() {
        return html`
            <link rel="stylesheet" href="${window.contentsCss}"/>

            ${this.notification || this.form}
        `;

    }

    firstUpdated(_changedProperties) {
        this.init();
    }

    onInputChanged = (ev) => {
        this.setData(ev);

        this.form = formHtml(this.data);

        this.resetValidation();
        if (this.hadSubmissionAttempt) {
            this.validate(false);
        }
    };

    setData = (ev) => {
        const key = ev.target.getAttribute('name');
        const value = ev.target.value;
        this.data = {
            ...this.data,
            [key]: value
        };
    };

    resetValidation() {
        const validationFeedbacks = this.shadowRoot.querySelectorAll('validation-feedback');
        validationFeedbacks.forEach(validationFeedback => validationFeedback.removeAttribute('message'));
    }

    validate(focus = false) {
        const validatableElements = Array.from(this.shadowRoot.querySelectorAll('[data-validation]')).map(element => ({
            name: element.getAttribute('name'),
            rules: element.dataset.validation.split(',')
        }));

        const errors = validate(validatableElements, this.data);
        if (errors.length > 0) {
            const {name, error} = errors[0];
            const validationFeedback = this.shadowRoot.querySelector(`.field_${name} validation-feedback`);
            if (validationFeedback) {
                validationFeedback.setAttribute('message', error);
            } else {
                console.warn('no validation-feedback for', name)
            }

            if (focus) {
                this.shadowRoot.querySelector(`[name="${name}"]`).focus();
            }

            this.hadSubmissionAttempt = false;

            return false;
        }

        return true;
    }

    submit() {
        this.hadSubmissionAttempt = true;
        if (this.validate(true)) {
            this.sendFormData(this.data)
                .then(() => {
                    window.history.pushState("success", null, "")
                    this.notification = document.createRange().createContextualFragment(successHtml);
                })
                .catch((e) => {
                    this.notification = e.toString();
                    throw e;
                });
        }
    }

    addFallbackForLabelForAttribute() {
        Object.keys(initial_schema.properties).forEach(key => {
            const input = this.shadowRoot.querySelector(`[name="${key}"]`);
            const labels = this.shadowRoot.querySelectorAll(`[for="${key}"]`);
            labels.forEach(label => label.addEventListener('click', ev => {
                input.focus();
            }, false))

        });
    }
}

customElements.define('form-handler', FormHandler);
