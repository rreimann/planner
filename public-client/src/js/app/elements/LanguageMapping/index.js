import { LitElement, css, html } from "lit-element";

import Choices from "utils/ChoicesWrapper.js";

class LanguageMapping extends LitElement {

  static languages = {
    "eng": "English",
    "fre": "French",
    "ita": "Italian",
    "deu": "German",
    "spa": "Spanish",
    "ara": "Arabic",
    "car": "Colloquial Arabic",
    "ben": "Bengali",
    "tir": "Tigrinya",
    "som": "Somali"
  }

  static levels = {
    0: "Basic Knowledge (A1-A2)",
    1: "Good (B1-B2)",
    2: "Fluent (C1-C2)",
    3: "Mother Language"
  }

  constructor() {
    super();
    this.languageMapping = {};
  }

  focus(){
    this.select.click();
  }

  static get styles() {
    return css`
      @keyframes slidein {
        from {
          height: 0;
        }
      }
      .container {
        margin-top: 20px;
      }
      .mapping {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 30px;
        animation: slidein 0.3s ease-in-out;
      }
      .mapping label {
        width: 4em;
      }
      .mapping label .value {
        color: #aaa;
      }
      .mapping .slider {
        display: block;
        vertical-align: middle;
        flex-grow: 1;
        margin: 0 15px;
      }
      .mapping .level {
        padding-left: 20px;
        width: 41%;
      }
      .mapping .slider paper-slider {
        display: inline;
        --paper-slider-active-color: var(--color-primary);
        --paper-slider-knob-color: var(--color-primary);
        --paper-slider-pin-color: var(--color-primary);
      }
    `;
  }

  static get properties() {
    return {
      languageMapping: { type: Object },
    };
  }

  get value() {
    return Object.entries(this.languageMapping).map(([language, points]) => {
      return { language, points };
    });
  }

  mappingValueChanged = (e) => {
    const { name, value } = e.target;

    this.languageMapping = {
      ...this.languageMapping,
      [name]: value,
    };
    this.emitChangeEvent();
  };

  emitChangeEvent() {
    this.dispatchEvent(
      new CustomEvent("change", { bubbles: true, detail: this.value })
    );
  }

  itemAdded(e) {
    this.languageMapping = {
      ...this.languageMapping,
      [e.detail.value]: 0,
    };
    this.emitChangeEvent();
  }

  itemRemoved(e) {
    const clone = { ...this.languageMapping };
    delete clone[e.detail.value];
    this.languageMapping = clone;
    this.emitChangeEvent();
  }

  render() {
    return html`
      <link rel="stylesheet" href="${window.contentsCss}" />

      <div class="container">
        <div class="fields">
          <div class="field">
            <select
              required
              multiple
              @addItem="${this.itemAdded}"
              @removeItem="${this.itemRemoved}"
            >
              <option value="" disabled>Please select</option>
              ${Object.entries(LanguageMapping.languages).map(
                ([code, name]) =>
                  html`<option value="${code}"
                    >${name}</option
                  >`
              )}
            </select>
          </div>

          <div class="field">
            ${Object.keys(this.languageMapping).map(
              (language) => html`
                <div class="mapping">
                  <label class="language">
                    ${LanguageMapping.languages[language]}
                  </label>
                  <div class="slider">
                    <paper-slider
                      @change="${this.mappingValueChanged}"
                      name="${language}"
                      value="${this.languageMapping[language]}"
                      max="3"
                      pin
                      snaps
                      max-markers="3"
                      steps="1"
                    />
                </div>
                <span class="level">${LanguageMapping.levels[this.languageMapping[language]]}</span>
              `
            )}
          </div>
        </div>
      </div>
    `;
  }

  firstUpdated(_changedProperties) {
    this.select = this.shadowRoot.querySelector("select");
    this.choices = new Choices(
      this.select,
      {
        removeItemButton: true,
        duplicateItemsAllowed: false,
      },
      this.shadowRoot
    );
  }
}

if (document.seawatchElementIsDefined_languageMapping === undefined) {
  customElements.define("language-mapping", LanguageMapping);
  document.seawatchElementIsDefined_languageMapping = true;
}
