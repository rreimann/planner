import {LitElement, html, unsafeCSS} from "lit-element";
import styles from './styles.cssfrag';

class TextareaCounter extends LitElement {
  constructor() {
    super();
    this.val = "";
    this.maxLength = 100;
    this.required = false;
    this._buttons = [];
  }

  focus(){
    this.shadowRoot.querySelector("textarea").focus();
  }

  static get properties() {
    return {
      val: { type: String },
      maxLength: { type: Number },
      required: { type: Boolean },
      buttons: { type: Array },
    };
  }

  get value() {
    return this.val;
  }

  set buttons(v){
    const old = this._buttons;
    this._buttons = v;
    this.requestUpdate('buttons', old);
  }

  static get styles() {
    return unsafeCSS(styles);
  }

  change(ev) {
    this.val = ev.target.value;

    const event = new CustomEvent("change", { bubbles: true });
    this.dispatchEvent(event);
  }

  render() {
    return html`
      <link rel="stylesheet" href="${window.contentsCss}" />

      <div class="textarea-with-counter">
        <aside>
            <div class="textarea-counter">${this.val.length}/${this.maxLength}</div>
            <div class="buttonlist">
                ${this._buttons.map(button => {
                  return html`<a href="${button.url}" target="_blank">${button.label}</a>`;
                })}
            </div>
        </aside>
        <textarea
          maxlength=${this.maxLength}
          placeholder="${this.getAttribute("placeholder")}"
          @input="${this.change}"
          required
        ></textarea>
      </div>
    `;
  }
}

customElements.define("textarea-counter", TextareaCounter);
