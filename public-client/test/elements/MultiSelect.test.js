import {fixture, expect, nextFrame} from "@open-wc/testing";
import {oneEvent} from "@open-wc/testing-helpers";

describe("MultiSelect", () => {
	it('should have a option for given property', async () => {
		// given
		let options = [
			{label: "One", value: "one"},
			{label: "Two", value: "two"}
		];

		let el = await fixture(`<multi-select></multi-select>`)
		el.options = options;

		await nextFrame();
		expect(el._options).to.deep.equal(options);

		const items = el.shadowRoot.querySelectorAll('.choices__item');
		expect(items).to.have.length(2);
		expect(items[0].innerText).to.equal(options[0].label);
		expect(items[1].dataset.value).to.equal(options[1].value);

	});

	it('should emit a bubbling change event', async () => {
		let el = await fixture(`<multi-select name="nationalities"></multi-select>`)
		let listener = oneEvent(el, "change")

		setTimeout(() => el.change())

		let event  = await listener

		expect(event).to.exist
		expect(event.bubbles).to.be.true
		expect(event.composed).to.be.true
	});

    it("displays buttons", async () => {
        const el = await fixture(`<textarea-counter></textarea-counter>`);
        const buttons = el.shadowRoot.querySelector('.buttonlist');

        el.buttons = [{label: 'test', url: ''}];

        await nextFrame();

        expect(buttons.innerText).to.equal('test');
        expect(buttons.children.length).to.equal(1);

    });


})
