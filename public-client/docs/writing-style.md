# Writing Style

Generally, we want to speak to the user in a pretty casual way.

Labels for text fields should be Title Case, headers should use sentence case
since they can get quite long.
