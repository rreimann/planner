FROM python:3.8-alpine as python_builder

RUN apk add build-base
WORKDIR /work
COPY public-backend/requirements.txt .
RUN sed -n '/^brotli/p' <requirements.txt > constraint.txt \
      && pip download brotli -c constraint.txt --no-binary :all: \
      && unzip Brotli-* \
      && rm *.zip \
      && (cd Brotli-* && python setup.py bdist_wheel)

RUN apk add postgresql-dev \
      && pip download psycopg2==2.8.5 --no-binary :all: \
      && tar -xf psycopg2-* \
      && rm *.tar.gz \
      && (cd psycopg2-* && python setup.py bdist_wheel)

FROM python:3.8-alpine

RUN apk add postgresql-libs

COPY --from=python_builder /work/Brotli-*/dist/Brotli-*.whl /tmp/
COPY --from=python_builder /work/psycopg2-*/dist/psycopg2-*.whl /tmp/

COPY backoffice/requirements.txt /tmp/requirements.txt
RUN pip install waitress /tmp/Brotli-*.whl /tmp/psycopg2-*.whl -r /tmp/requirements.txt

COPY backoffice /srv/backoffice
WORKDIR /srv/backoffice

ENV \
  PYTHONPATH=/srv/backoffice \
  DJANGO_SETTINGS_MODULE=seawatch_planner.settings.prod

RUN printf \
      '#!/bin/sh\nexec python /srv/backoffice/manage.py migrate' \
      > /usr/local/bin/migrate && \
      chmod +x /usr/local/bin/migrate

RUN env ALLOWED_HOSTS= SECRET_KEY=irrelevant EMAIL_URL=consolemail:// ADMINS= \
      ERROR_EMAIL_FROM=root@localhost PLANNER_PUBLIC_BACKEND_URL=http://example.com \
      DATABASE_URL= PLANNER_PUBLIC_BACKEND_AUTH_TOKEN= \
      /srv/backoffice/manage.py collectstatic

# TODO compilemessages

EXPOSE 8080/tcp

CMD ["waitress-serve", "--port=8080", "seawatch_planner.wsgi:application"]
