from django.views.generic.base import TemplateView

from ..models import Position


class IndexView(TemplateView):
    nav_item = "index"
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data["all_positions"] = Position.objects.all()
        return context_data
