import django_tables2 as tables

from .models import Application


class OpenApplicationsTable(tables.Table):
    name = tables.Column(linkify=True)

    class Meta:
        model = Application
        template_name = "django_tables2/bootstrap.html"
        fields = ("name", "requested_positions")
