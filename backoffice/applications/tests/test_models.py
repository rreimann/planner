from pytest import mark

from .factories import ApplicationFactory


@mark.django_db
def test_get_absolute_url_includes_id():
    application = ApplicationFactory(id=94)
    url = application.get_absolute_url()
    assert url == "/applications/94"
