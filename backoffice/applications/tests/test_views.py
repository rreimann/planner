from typing import Optional
from unittest.mock import Mock, patch

import pytest
from bs4 import BeautifulSoup
from django.contrib.messages import constants as message_levels
from django.core.exceptions import PermissionDenied
from django.http.response import Http404, HttpResponseNotAllowed, HttpResponseRedirect
from django.shortcuts import reverse
from django.test import Client, RequestFactory
from pytest import mark, raises
from pytest_django.asserts import assertContains, assertNotContains, assertRedirects

from seawatch_planner.tests.asserts import assert_requires_permissions
from seawatch_planner.tests.messages import StubMessageStorage
from seawatch_registration.tests.factories import PositionFactory, UserFactory

from ..models import Application, ImportedApplication, Volunteer
from ..views import (
    ApplicationDeleteView,
    ApplicationDetailView,
    ApplicationListView,
    ImportApplicationView,
    VolunteerListView,
    accept_application,
)
from .factories import (
    ApplicationFactory,
    ImportedApplicationDTOFactory,
    ImportedApplicationFactory,
    ImportedLanguageFactory,
    VolunteerFactory,
)


@mark.django_db
def test_application_list_is_reachable(client: Client):
    client.force_login(UserFactory(permission="applications.view_application"))
    response = client.get("/applications/")
    assertContains(response, "Applications")


@mark.django_db
def test_application_list_requires_permission(rf: RequestFactory):
    assert_requires_permissions(
        rf, ApplicationListView, ["applications.view_application"]
    )


@mark.django_db
def test_application_list_empty(rf: RequestFactory):
    request = rf.get("ignored")
    request.user = UserFactory(view_permissions=ApplicationListView)
    response = ApplicationListView.as_view()(request)

    assertContains(response, "No applications found")


@mark.django_db
def test_application_list(rf: RequestFactory):
    ApplicationFactory(name="foo")
    ApplicationFactory(name="bar")

    request = rf.get("ignored")
    request.user = UserFactory(view_permissions=ApplicationListView)
    response = ApplicationListView.as_view()(request)

    assertContains(response, "foo")
    assertContains(response, "bar")


@mark.django_db
def test_applications_by_position(rf: RequestFactory):
    ApplicationFactory(
        name="this position", requested_positions=[PositionFactory(name="captain")]
    )

    ApplicationFactory(
        name="other position", requested_positions=[PositionFactory(name="chef")]
    )

    request = rf.get("ignored", {"position": "captain"})
    request.user = UserFactory(view_permissions=ApplicationListView)
    response = ApplicationListView.as_view()(request)

    assertContains(response, "this position")
    assertNotContains(response, "other position")


@mark.django_db
def test_application_details_requires_permissions(rf: RequestFactory):
    ApplicationFactory(id=123)
    request = rf.get("/")
    request.user = UserFactory()

    with raises(PermissionDenied):
        ApplicationDetailView.as_view()(request, pk=123)


@mark.django_db
def test_application_details(rf: RequestFactory):
    ApplicationFactory(
        id=1764,
        name="Peter pan-pizza",
        requested_positions=[
            PositionFactory(name="pirate"),
            PositionFactory(name="pizza cook"),
        ],
    )

    request = rf.get("/")
    request.user = UserFactory(permission="view_application")

    response = ApplicationDetailView.as_view()(request, pk=1764)

    assertContains(response, "Peter pan-pizza")
    assertContains(response, "pirate")
    assertContains(response, "pizza cook")


@mark.django_db
def test_application_details_reachable(client: Client):
    ApplicationFactory(id=1234)
    client.force_login(UserFactory(permission="view_application"))
    response = client.get(reverse("application_detail", kwargs={"pk": 1234}))
    assert response.status_code == 200


@mark.django_db
def test_import_application_requires_permission(rf: RequestFactory):
    assert_requires_permissions(
        rf, ImportApplicationView, ["view_application", "add_application"]
    )


@mark.django_db
def test_import_application_is_reachable(client: Client):
    client.force_login(UserFactory(view_permissions=ImportApplicationView))
    with patch("applications.views.import_application") as import_mock:
        import_mock.return_value = None
        response = client.get("/applications/import")
    assertContains(response, "Import Applications")


@mark.django_db
def test_import_application_acknowledges_import(rf: RequestFactory):
    request = rf.get("/applications/import")
    request.user = UserFactory(view_permissions=ImportApplicationView)
    acknowledge_import_mock = Mock()
    ImportApplicationView.as_view(
        import_application=lambda: ImportedApplicationDTOFactory(uid="foo"),
        acknowledge_import=acknowledge_import_mock,
    )(request)
    assert acknowledge_import_mock.call_count == 1
    acknowledge_import_mock.assert_called_with("foo")


@mark.django_db
def test_import_application_shows_application(rf: RequestFactory):
    request = rf.get("/")
    request.user = UserFactory(view_permissions=ImportApplicationView)
    name = "Hiero Protagonist"

    response = ImportApplicationView.as_view(
        import_application=lambda: ImportedApplicationDTOFactory(name=name),
        acknowledge_import=Mock(),
    )(request)
    assertContains(response, f"Name: {name}")
    assert ImportedApplication.objects.get(name=name)


@mark.django_db
def test_import_application_has_languages(rf: RequestFactory):
    # given
    request = rf.get("/")
    application = ImportedApplicationDTOFactory()
    application_languages = list(map(lambda e: e.code, application.spoken_languages))

    # when
    request.user = UserFactory(view_permissions=ImportApplicationView)
    response = ImportApplicationView.as_view(
        import_application=lambda: application, acknowledge_import=Mock(),
    )(request)

    # then
    imported_application = ImportedApplication.objects.last()
    assert imported_application.languages.count() == len(application.spoken_languages)
    for language in application.spoken_languages:
        assert imported_application.languages.get(
            code=language.code, points=language.points
        )


@mark.django_db
def test_import_application_shows_message_if_no_new_application(rf: RequestFactory):
    request = rf.get("/")
    request.user = UserFactory(view_permissions=ImportApplicationView)

    response = ImportApplicationView.as_view(import_application=lambda: None)(request)

    assertContains(response, "No new applications")


@mark.django_db
def test_import_application_page_doesnt_import_with_ImportedApplication_in_db(
    rf: RequestFactory,
):
    ImportedApplicationFactory()
    request = rf.get("/")
    request.user = UserFactory(view_permissions=ImportApplicationView)

    ImportApplicationView.as_view(
        import_application=lambda: pytest.fail(
            "Application was imported even though there already is an "
            "ImportedApplication in the database."
        )
    )(request)


@mark.django_db
def test_application_details_linked_from_list(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory(permission="view_application")

    ApplicationFactory(id=7842, name="Molly Millions")

    response = ApplicationListView.as_view()(request)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    assert html.find(
        "a",
        href=reverse("application_detail", kwargs={"pk": 7842}),
        string=lambda s: s.strip() == "Molly Millions",
    )


@mark.django_db
def test_accept_button_exists(rf: RequestFactory):
    request = rf.get("/")
    request.user = UserFactory(view_permissions=ImportApplicationView)

    response = ImportApplicationView.as_view(
        import_application=lambda: ImportedApplicationDTOFactory(
            name="Hiero Protagonist"
        ),
        acknowledge_import=Mock(),
    )(request)

    html = BeautifulSoup(response.rendered_content, features="html.parser")

    form = html.find("form", method="POST", action=reverse("application_import"))
    assert form
    assert form.find("button", type="submit", text="Import application")


@mark.django_db
def test_no_accept_button_if_no_new_applications(rf: RequestFactory):
    request = rf.get("/")
    request.user = UserFactory(view_permissions=ImportApplicationView)

    response = ImportApplicationView.as_view(import_application=lambda: None)(request)

    html = BeautifulSoup(response.rendered_content, features="html.parser")

    buttons = html("button", text="Import application")
    assert len(buttons) == 0


@mark.django_db
def test_importing_unknown_application_produces_404(rf: RequestFactory):
    request = rf.post(reverse("application_import"), {"application_id": 1312})
    request.user = UserFactory(view_permissions=ImportApplicationView)
    with pytest.raises(Http404):
        response = ImportApplicationView.as_view()(request)


@mark.django_db
def test_importing_creates_application(rf: RequestFactory):
    name = "yip yip yip"
    phone = "+319978698459"
    PositionFactory(name="leftposition")
    PositionFactory(name="rightposition")
    ImportedApplicationFactory(
        id=1312, name=name, phone_number=phone, positions="leftposition;rightposition"
    )

    request = rf.post(reverse("application_import"), {"application_id": 1312})
    request.user = UserFactory(view_permissions=ImportApplicationView)
    request._messages = StubMessageStorage()

    response = ImportApplicationView.as_view()(request)

    application = Application.objects.get(name=name, phone_number=phone)
    assert [pos.name for pos in application.requested_positions.all()] == [
        "leftposition",
        "rightposition",
    ]


@mark.django_db
def test_import_with_languages(rf: RequestFactory):
    # given
    imported_language = ImportedLanguageFactory()

    # when
    request = rf.post(
        reverse("application_import"),
        {"application_id": imported_language.application.id},
    )
    request.user = UserFactory(view_permissions=ImportApplicationView)
    request._messages = StubMessageStorage()
    ImportApplicationView.as_view()(request)

    # then
    assert Application.objects.last().languages.count() == 1
    assert Application.objects.last().languages.last().code == imported_language.code
    assert (
        Application.objects.last().languages.last().points == imported_language.points
    )


@mark.django_db
def test_importing_shows_message(rf: RequestFactory):
    name = "yip yip yip"
    ImportedApplicationFactory(id=1312, name=name)
    request = rf.post(reverse("application_import"), {"application_id": 1312})
    request.user = UserFactory(view_permissions=ImportApplicationView)
    request._messages = StubMessageStorage()
    response = ImportApplicationView.as_view()(request)
    assert request._messages.messages == [
        (message_levels.SUCCESS, f"{name}'s application has been imported", "")
    ]


@mark.django_db
def test_delete_application_reachable(client: Client):
    ApplicationFactory(id=42)
    client.force_login(UserFactory(permission="delete_application"))
    response = client.get(reverse("application_delete", kwargs={"pk": 42}))

    assert response.status_code == 200


@mark.django_db
def test_delete_application_confirmation(rf: RequestFactory):
    ApplicationFactory(id=24, name="Cayce Pollard")

    request = rf.get("")
    request.user = UserFactory(permission="delete_application")

    response = ApplicationDeleteView.as_view()(request, pk=24)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    form = html.find("form", action=False, method="post")
    assert "Cayce Pollard's application" in form.text


@mark.django_db
def test_delete_application(rf: RequestFactory):
    ApplicationFactory(id=9123)

    request = rf.post("")
    request.user = UserFactory(permission="delete_application")

    response = ApplicationDeleteView.as_view()(request, pk=9123)

    assert isinstance(response, HttpResponseRedirect)
    assert response.url == reverse("application_list")
    assert Application.objects.filter(id=9123).count() == 0


@mark.django_db
def test_delete_application_requires_permisson(rf: RequestFactory):
    request = rf.post("")
    request.user = UserFactory()

    with raises(PermissionDenied):
        ApplicationDeleteView.as_view()(request, pk=5)


@mark.django_db
def test_delete_application_from_details(rf: RequestFactory):
    ApplicationFactory(id=4332)

    request = rf.get("")
    request.user = UserFactory(
        permissions=["applications.view_application", "applications.delete_application"]
    )

    response = ApplicationDetailView.as_view()(request, pk=4332)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    assert html.find(
        "a",
        href=reverse("application_delete", kwargs={"pk": 4332}),
        string=lambda s: s.strip() == "Delete application",
    )


@mark.django_db
def test_accept_application(rf: RequestFactory):
    phone = "+49877492099"
    ApplicationFactory(id=7391, name="Yours Truly", phone_number=phone)

    request = rf.post("")
    request.user = UserFactory(
        permissions=["applications.delete_application", "applications.add_volunteer"]
    )

    response = accept_application(request, pk=7391)

    assert isinstance(response, HttpResponseRedirect)
    assert response.url == reverse("application_list")
    assert Application.objects.filter(pk=7391).count() == 0
    assert Volunteer.objects.filter(name="Yours Truly", phone_number=phone).exists()


@mark.django_db
def test_accept_application_reachable(client: Client):
    ApplicationFactory(id=1)

    client.force_login(
        UserFactory(
            permissions=[
                "applications.delete_application",
                "applications.add_volunteer",
            ]
        )
    )
    response = client.post(reverse("application_accept", kwargs={"pk": 1}))
    assertRedirects(
        response, reverse("application_list"), fetch_redirect_response=False
    )


@mark.django_db
def test_accept_requires_post(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory(
        permissions=["applications.delete_application", "applications.add_volunteer"]
    )

    response = accept_application(request, pk=42)

    assert isinstance(response, HttpResponseNotAllowed)


@mark.django_db
def test_requires_permissions(rf: RequestFactory):
    # Missing "delete_application"
    ApplicationFactory(id=82)
    request = rf.post("")
    request.user = UserFactory(permission="applications.add_volunteer")

    with raises(PermissionDenied):
        accept_application(request, pk=82)

    # Missing "add_volunteer"
    ApplicationFactory(id=28)
    request = rf.post("")
    request.user = UserFactory(permission="applications.delete_application")

    with raises(PermissionDenied):
        accept_application(request, pk=28)


@mark.django_db
def test_accept_application_from_details_page(rf: RequestFactory):
    ApplicationFactory(id=812)

    request = rf.get("")
    request.user = UserFactory(permission="applications.view_application")

    response = ApplicationDetailView.as_view()(request, pk=812)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    form = html.find(
        "form", method="post", action=reverse("application_accept", kwargs={"pk": 812})
    )
    assert form
    assert form.find("input", attrs={"name": "csrfmiddlewaretoken"})
    assert form.find("input", type="submit", value="Accept application")


@mark.django_db
def test_volunteer_list_is_reachable(client: Client):
    client.force_login(UserFactory(permission="applications.view_volunteer"))
    response = client.get(reverse("volunteer_list"))
    assert response.status_code == 200


@mark.django_db
def test_volunteer_list_has_volunteers(rf: RequestFactory):
    VolunteerFactory(name="Harry Potter")
    request = rf.get("")
    request.user = UserFactory(permission="applications.view_volunteer")

    response = VolunteerListView.as_view()(request)
    assert "Harry Potter" in response.rendered_content


@mark.django_db
def test_volunteer_list_requires_permission(rf: RequestFactory):
    assert_requires_permissions(rf, VolunteerListView, ["applications.view_volunteer"])


@mark.django_db
def test_volunteer_list_items_show_button_to_request_availability(rf: RequestFactory):
    VolunteerFactory(name="Mail Gebson")
    request = rf.get("")
    request.user = UserFactory(permission="applications.view_volunteer")
    response = VolunteerListView.as_view()(request)

    html = BeautifulSoup(response.rendered_content, features="html.parser")

    form = html.find("form", method="post", action=reverse("volunteer_list"))
    assert form.find("button", type="submit", text="Request Availabilities")


@mark.django_db
def test_volunteer_request_availabilty_button_requests_email_to_be_sent(
    rf: RequestFactory,
):
    name = "Emailuel Kant"
    email = "mail@reine-vernunft.de"
    VolunteerFactory(name=name, email=email)
    request = rf.post(reverse("volunteer_list"), {"request_availability": name})
    request.user = UserFactory(permission="applications.view_volunteer")
    request_availabilities_mock = Mock()
    response = VolunteerListView.as_view(
        request_availabilities=request_availabilities_mock
    )(request)
    assert request_availabilities_mock.call_count == 1
    request_availabilities_mock.assert_called_with(email)
