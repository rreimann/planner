from bs4 import BeautifulSoup
from django.shortcuts import reverse
from django.test import Client
from pytest import mark
from seawatch_registration.tests.factories import UserFactory

from .factories import ApplicationFactory, VolunteerFactory


@mark.django_db
def test_applications_admin(client: Client):
    client.force_login(UserFactory(is_staff=True, permission="view_application"))
    assert (
        client.get(reverse("admin:applications_application_changelist")).status_code
        == 200
    )


@mark.django_db
def test_applicant_name_in_list(client: Client):
    ApplicationFactory(id=814, name="Foo Barman")
    client.force_login(UserFactory(is_staff=True, permission="view_application"))

    response = client.get(reverse("admin:applications_application_changelist"))
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    detailsUrl = reverse(
        "admin:applications_application_change", kwargs={"object_id": 814}
    )
    links = html.find_all("a", href=detailsUrl)
    assert len(links) == 1
    assert links[0].text == "Foo Barman"


@mark.django_db
def test_volunteer_admin(client: Client):
    client.force_login(UserFactory(is_staff=True, permission="view_volunteer"))
    assert (
        client.get(reverse("admin:applications_volunteer_changelist")).status_code
        == 200
    )


@mark.django_db
def test_volunteer_name_in_list(client: Client):
    name = "Reason"
    VolunteerFactory(id=94, name=name)
    client.force_login(UserFactory(is_staff=True, permission="view_volunteer"))

    response = client.get(reverse("admin:applications_volunteer_changelist"))
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    detailsUrl = reverse(
        "admin:applications_volunteer_change", kwargs={"object_id": 94}
    )

    links = html.find_all("a", href=detailsUrl)
    assert len(links) == 1
    assert links[0].text == name
