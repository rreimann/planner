import uuid
from datetime import datetime
from urllib.request import Request

from django.conf import settings
from pytest_httpserver import HTTPServer

from applications.service import Language, import_application


def test_import_with_mandatory_fields(httpserver: HTTPServer):
    # given
    settings.PLANNER_PUBLIC_BACKEND_URL = httpserver.url_for("")
    settings.PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = "insecure-preshared-token-for-dev"

    # a default response from public-backend
    httpserver.expect_request("/applications/oldest").respond_with_json(
        {
            "uid": str(uuid.uuid1()),
            "created": str(datetime.now()),
            "data": {
                "first_name": "Harry P.",
                "phone_number": "+3914568179",
                "spoken_languages": [{"language": "deu", "points": 3}],
                "positions": ["2nd-officer", "chief-eng"],
            },
        }
    )

    # when we import latest applicants
    applicant = import_application()

    # latest applicant should be correct
    assert applicant is not None
    assert applicant.name == "Harry P."
    assert applicant.phone_number == "+3914568179"
    assert applicant.spoken_languages == [Language(code="deu", points=3)]
    assert applicant.positions == ["2nd-officer", "chief-eng"]
