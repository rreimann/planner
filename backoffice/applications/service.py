import datetime
import json
from dataclasses import dataclass
from types import SimpleNamespace
from typing import List, Optional
from urllib.parse import quote

import requests
from django.conf import settings


@dataclass
class Language:
    code: str
    points: int


@dataclass
class ImportedApplicationDTO:
    name: str
    uid: str
    created: datetime.datetime
    phone_number: str
    spoken_languages: List[Language]
    positions: List[str]


def auth_headers():
    # As per http spec, we need to specify an authentication scheme. There isn't one for
    # pre-shared tokens (Bearer is commonly used, but that implies OAuth2), so we just
    # use Token.
    return {"Authorization": f"Token {settings.PLANNER_PUBLIC_BACKEND_AUTH_TOKEN}"}


def import_application() -> Optional[ImportedApplicationDTO]:
    response = requests.get(
        settings.PLANNER_PUBLIC_BACKEND_URL + "/applications/oldest",
        headers=auth_headers(),
    )
    response.raise_for_status()
    if response.status_code == 204:
        return None
    else:
        data = response.json()
        return ImportedApplicationDTO(
            uid=data["uid"],
            created=data["created"],
            name=data["data"]["first_name"],
            phone_number=data["data"]["phone_number"],
            spoken_languages=to_language_list(data["data"]["spoken_languages"]),
            positions=data["data"]["positions"],
        )


def request_availabilities(email) -> None:
    response = requests.get(
        settings.PLANNER_PUBLIC_BACKEND_URL + "/applications/request_availabilities",
        headers=auth_headers(),
        params={"email": email},
    )
    response.raise_for_status()


def acknowledge_import(application_id) -> None:
    response = requests.delete(
        settings.PLANNER_PUBLIC_BACKEND_URL + "/applications/" + quote(application_id),
        headers=auth_headers(),
    )
    response.raise_for_status()


def to_language_list(languages) -> List[Language]:
    return list(
        map(lambda e: Language(code=e["language"], points=e["points"]), languages,)
    )
