from django.db import models
from django.shortcuts import reverse
from phonenumber_field.modelfields import PhoneNumberField

from seawatch_registration.models import Position

from .service import ImportedApplicationDTO


class Application(models.Model):
    name = models.CharField(max_length=255)
    phone_number = PhoneNumberField()
    requested_positions = models.ManyToManyField(Position)
    # TODO: Propper Email field
    email = models.CharField(max_length=255)

    @classmethod
    def create_from_imported_application(cls, imported_application):
        result = cls(
            name=imported_application.name,
            phone_number=imported_application.phone_number,
        )
        result.save()
        result.requested_positions.set(
            [
                Position.objects.get_or_create(name=position)[0]
                for position in imported_application.positions.split(";")
            ]
        )
        return result

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("application_detail", kwargs={"pk": self.pk})


class Language(models.Model):
    code = models.CharField(max_length=3)
    points = models.IntegerField()

    application = models.ForeignKey(
        Application, on_delete=models.CASCADE, related_name="languages"
    )


class ImportedApplication(models.Model):
    uid = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    phone_number = PhoneNumberField()
    created = models.DateTimeField(editable=False)
    positions = models.TextField()

    @classmethod
    def from_data_transfer_object(cls, dto: ImportedApplicationDTO):
        return cls(
            name=dto.name,
            uid=dto.uid,
            created=dto.created,
            phone_number=dto.phone_number,
            positions=";".join(dto.positions),
        )


class ImportedLanguage(models.Model):
    code = models.CharField(max_length=3)
    points = models.IntegerField()

    application = models.ForeignKey(
        ImportedApplication, on_delete=models.CASCADE, related_name="languages"
    )


class Volunteer(models.Model):
    name = models.CharField(max_length=255)
    phone_number = PhoneNumberField()
    # TODO: Propper Email field
    email = models.CharField(max_length=255)

    def __str__(self):
        return self.name
