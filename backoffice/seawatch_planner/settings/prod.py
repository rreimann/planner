import email.utils

from .base import *

DEBUG = False

assert MIDDLEWARE[0].endswith(
    ".SecurityMiddleware"
), "MIDDLEWARE setup isn't as expected"

MIDDLEWARE = [
    MIDDLEWARE[0],
    "whitenoise.middleware.WhiteNoiseMiddleware",
    *MIDDLEWARE[1:],
]


# Must contain a list of all public hostnames for the service as a comma-separated list.
# See https://docs.djangoproject.com/en/3.0/ref/settings/#allowed-hosts
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS")

SECRET_KEY = env.str("SECRET_KEY")

# EMAIL_URL="smtp://user:password@hostname:25". See EMAIL_SCHEMES at
# https://github.com/joke2k/django-environ/blob/master/environ/environ.py for more
# details,
vars().update(env.email_url("EMAIL_URL"))

#  Add configuration for static files storage using whitenoise
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

MEDIA_ROOT = "media/"

PLANNER_PUBLIC_BACKEND_URL = env.str("PLANNER_PUBLIC_BACKEND_URL")
PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = env.str("PLANNER_PUBLIC_BACKEND_AUTH_TOKEN")

# ADMINS=Full Name <email-with-name@example.com>,anotheremailwithoutname@example.com
ADMINS = email.utils.getaddresses([env("ADMINS")])

SERVER_EMAIL = env("ERROR_EMAIL_FROM")

DATABASES = {"default": env.db()}
