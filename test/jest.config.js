const PUBLIC_FRONTEND_URL = process.env.PUBLIC_FRONTEND_URL || 'http://localhost:8001';
const PUBLIC_BACKEND_URL = process.env.PUBLIC_BACKEND_URL || 'http://localhost:8001';
const BACKOFFICE_URL = process.env.BACKOFFICE_URL || 'http://localhost:8000';
const WEBMAIL_URL = process.env.WEBMAIL_URL || 'http://localhost:8085';
const headless = process.env.HEADLESS ? process.env.HEADLESS.toLowerCase() === 'true' :  true;
const BACKOFFICE_ADMIN_USERNAME = process.env.BACKOFFICE_ADMIN_USERNAME || 'admin';
const BACKOFFICE_ADMIN_PASSWORD = process.env.BACKOFFICE_ADMIN_PASSWORD || 'admin';

module.exports = {
    "globals": {
        "TIMEOUT": 32 * 1000,
        "PUBLIC_FRONTEND_URL": PUBLIC_FRONTEND_URL,
        "PUBLIC_BACKEND_URL": PUBLIC_BACKEND_URL,
        "BACKOFFICE_URL": BACKOFFICE_URL,
        "WEBMAIL_URL": WEBMAIL_URL,
        "LAUNCH_OPTIONS": {
          headless: headless,
          args: ["--no-sandbox", "--disable-setuid-sandbox"],
        },
        BACKOFFICE_ADMIN_USERNAME,
        BACKOFFICE_ADMIN_PASSWORD,
    }
};
