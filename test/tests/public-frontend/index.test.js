const puppeteer = require('puppeteer');
const jestscreenshot = require('@jeeyah/jestscreenshot');
const path = require('path');

describe('Public Frontend', () => {
    let browser = null;
    let page = null;

    beforeAll(async () => {
        browser = await puppeteer.launch(LAUNCH_OPTIONS);
        page = await browser.newPage();

        await jestscreenshot.init({
            page: page,
            dirName: __dirname,
            scriptName: path.basename(__filename).replace('.js', ''),
        });
    });

    it('is up', async () => {
        page.goto(PUBLIC_FRONTEND_URL);
        await page.waitForSelector('#anchor', {visible: true});
    }, TIMEOUT);

    it('renders the initial form with empty inputs', async () => {
        await page.goto(PUBLIC_FRONTEND_URL);
        const valueFirstName = await page.$eval('form-handler', e => e.shadowRoot.querySelector('input[name="first_name"]').value);
        expect(valueFirstName).toBe('');
        const valuePhone = await page.$eval('form-handler', e => e.shadowRoot.querySelector('input[name="phone_number"]').value);
        expect(valuePhone).toBe('');
    }, TIMEOUT);

    afterAll(() => {
        jestscreenshot.cleanup(function () {
            if (browser) {
                browser.close();
            }
        });
    });
});
